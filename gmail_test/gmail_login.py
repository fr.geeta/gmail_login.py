from selenium import webdriver
import unittest
import time

class LoginTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path="d:/Python, Selenium/Drivers/chromedriver.exe")
        self.driver.maximize_window()
        self.driver.implicitly_wait(20)

    def test_login_in_gmailbox_send_email_confirm_receipt(self):
        driver = self.driver
        driver.get("https://mail.google.com/")

        email_field = driver.find_element_by_id("identifierId").send_keys("test.052319")
        next_button = driver.find_element_by_id("identifierNext").click()
        password_field = driver.find_element_by_name("password").send_keys("passpass1!")
        next_button2 = driver.find_element_by_id("passwordNext").click()

        new_email_button = driver.find_element_by_class_name("z0").click()
        send_to_field = driver.find_element_by_name("to").send_keys("test.052319@gmail.com")
        subject_field = driver.find_element_by_name("subjectbox").send_keys("test email")
        message_field = driver.find_element_by_xpath("//div[@class='Am Al editable LW-avf']").send_keys("some test text here")
        send_button = driver.find_element_by_xpath("//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']").click()

        time.sleep(5)

        email_sender_name = driver.find_element_by_xpath("//span[@class='zF']")
        email_received_subject = driver.find_element_by_xpath("//span[@class='bog']")
        return email_received_subject.text
        email_received_text_body = driver.find_element_by_xpath("//span[@class='y2'")


        assert sender_name.text == "me"
        assert email_received_subject.text == "test email"
        assert email_received_text_body.text == "some test text here"

    def tearDown(self):
        self.driver.quit()